package com.example.demo.facturas;

import javax.persistence.*;

@Entity(name = "Facturas")
@NamedQuery(name="FacturaModel.findByImporte", query="from com.example.demo.facturas.FacturaModel where importe > ?1")
public class FacturaModel {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;
    private String fecha;
    private String cliente;
    private int id_producto;
    private double importe;

    public FacturaModel() {
    }

    public FacturaModel(int id, String fecha, String cliente, int idProducto, double importe) {
        this.id = id;
        this.fecha = fecha;
        this.cliente = cliente;
        this.id_producto = idProducto;
        this.importe = importe;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public int getIdProducto() {
        return id_producto;
    }

    public void setIdProducto(int idProducto) {
        this.id_producto = idProducto;
    }

    public double getImporte() {
        return importe;
    }

    public void setImporte(double importe) {
        this.importe = importe;
    }
}
